
collections:
	ansible-galaxy collection install community.general

venv:
	pipenv install --dev

shell:
	pipenv shell

playbook:
	ansible-playbook -i node1,node2, playbook.yml -t all -v